# MythMaker

A tool to create cards for the board game [Myth](https://boardgamegeek.com/boardgame/140519/myth).

Written with C# + WPF.

![](./mythmaker.gif)
